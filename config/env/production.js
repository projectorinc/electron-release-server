/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  appUrl: process.env.APP_URL,
  models: {
    connection: 'mysql',
    migrate: 'safe'
  },

  auth: {
    // Provide a set of credentials that can be used to access the admin interface.
    static: {
      username: process.env.ADMIN_USERNAME,
      password: process.env.ADMIN_PASSWORD
    },
    // You can also specify an ldap connection that can be used for authentication.
    //ldap: {
    //  usernameField: 'USERNAME_FIELD', // Key at which the username is stored
    //  server: {
    //    url: 'ldap://LDAP_SERVER_FQDN:389',
    //    bindDn: 'INSERT_LDAP_SERVICE_ACCOUNT_USERNAME_HERE',
    //    bindCredentials: 'INSERT_PASSWORD_HERE',
    //    searchBase: 'USER_SEARCH_SPACE', // ex: ou=Our Users,dc=companyname,dc=com
    //    searchFilter: '(USERNAME_FIELD={{username}})'
    //  }
    //}
  },

  connections: {
    mysql: {
      adapter: 'sails-mysql',
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DB
    },
    redis: {
      adapter: 'sails-redis',
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
      password: process.env.REDIS_PASS,
      database: process.env.REDIS_DB_SNAPSHOT
    }
  },

  session: {
    cookie: {
      maxAge: 14 * 24 * 60 * 60 * 1000
    },
    adapter: 'redis',
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    ttl: process.env.REDIS_TTL,
    db: process.env.REDIS_DB,
    pass: process.env.REDIS_PASS,
    prefix: process.env.REDIS_PREFIX
  },

  aws : {
    bucket : process.env.AWS_S3_BUCKET,
    ak : process.env.AWS_S3_KEY,
    sk : process.env.AWS_S3_SECRET,
    cloudfrontURL : null,
    s3URL : 'https://s3.amazonaws.com/'
  },

  /***************************************************************************
   * Set the port in the production environment to 80                        *
   ***************************************************************************/

  port: process.env.PORT,

  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  // log: {
  //   level: "silent"
  // }

  // auth: {
  //   secret: 'temppass'
  // }

};
